# frozen_string_literal: true

module Api
  class PollsController < ApiController
    include JSONAPI::ActsAsResourceController
  end
end

# frozen_string_literal: true

class UsersController < DeviseTokenAuth::SessionsController
  def create
    # Translate ember-simple-auth fields to devise_token_auth
    params.require(:user).permit(:email, :password)
    params[:email] = params[:user][:email]
    params[:password] = params[:user][:password]
  end
end

# frozen_string_literal: true

module Api
  class OrganizationResource < Api::BaseResource
    abstract
    include JSONAPI::Authorization::PunditScopedResource

    model_name 'Organization'

    attribute :name
  end
end

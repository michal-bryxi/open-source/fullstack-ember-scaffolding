# frozen_string_literal: true
module Api
  class BaseResource < JSONAPI::Resource
    abstract
    include JSONAPI::Authorization::PunditScopedResource
  end
end
# frozen_string_literal: true

module Api
  class OrganizationDependentResource < Api::BaseResource
    has_one :organization

    # Resources that inherit from this one all need to update .organization_id prior to saving new record
    before_save do
      user = context[:current_user]

      @model.organization_id = user.organization_id if @model.new_record?
    end
  end
end
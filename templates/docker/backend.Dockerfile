FROM ruby:#{RUBY_VERSION}-alpine
ENV DOCKERIZE_VERSION v0.6.1

RUN apk update && \
      apk add --no-cache \
      git \
      g++ \
      make \
      graphviz \
      ttf-freefont \
      linux-headers \
      postgresql-dev \
      ncurses \
      openssl

RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz

WORKDIR /myapp

ADD backend/Gemfile backend/Gemfile.lock /myapp/
RUN bundle install

# ENTRYPOINT ["dockerize", "-wait", "tcp://db:5432", "-timeout", "60s"]

# CMD dockerize -wait tcp://db:5432 bundle exec rails s --port 3000 --binding 0.0.0.0 -P /dev/null
import Route from "@ember/routing/route";
import ApplicationRouteMixin from "ember-simple-auth/mixins/application-route-mixin";
import { inject as service } from '@ember/service';
// import config from 'common-components/config';

export default class ApplicationRoute extends Route.extend(
  ApplicationRouteMixin
) {
  @service intl;

  routeAfterAuthentication = 'authenticated';

  beforeModel() {
    this._super(...arguments);

    // this.intl.setLocale([config.locale]);
  }
}

import JSONAPIAdapter from '@ember-data/adapter/json-api';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import DataAdapterMixin from "ember-simple-auth/mixins/data-adapter-mixin";
import config from "../config/environment";

export default class ApplicationAdapter extends JSONAPIAdapter.extend(DataAdapterMixin) {
  @service session;

  namespace = "api";
  host = config.APP.API_HOST;

  @computed('session.data.authenticated.token', 'session.isAuthenticated')
  get headers() {
    let headers = {};

    if (this.session.isAuthenticated) {
      let { email, token, client } = this.session.data.authenticated;
      // headers['Authorization'] = `Token token="${token}", email="${email}"`;
      headers["access-token"] = token;
      headers["uid"] = email;
      headers["client"] = client;
    }

    return headers;
  }
};

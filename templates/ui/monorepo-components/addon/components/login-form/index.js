import Component from "@glimmer/component";
import { inject as service } from "@ember/service";
import { task } from "ember-concurrency";
import UserValidations from "monorepo-components/validations/user";

export default class LoginFormComponent extends Component {
  @service session;
  UserValidations = UserValidations;
  model = { username: "", password: "" };

  @task(function* (credentials) {
    this.session.authenticate(
      "authenticator:devise-token-auth",
      credentials.get("username"),
      credentials.get("password")
    );
  })
  login;
}

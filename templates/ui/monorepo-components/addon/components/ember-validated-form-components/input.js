import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class EmberValidatedFormComponentsInputComponent extends Component {
  @action
  onInput(event) {
    this.args.update(event.target.value);
  }
}
